
import React from 'react';
import { PersistGate } from 'redux-persist/integration/react'
import { SafeAreaView, StatusBar } from 'react-native'
import { persistor, store } from './src/stateManagenent/utils/store'
import Routing from './src/Routing';
import { Provider } from 'react-redux'



const App: () => Node = () => {
  return (
    <Provider store={store}>
       <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={{flex:1}}>
          <StatusBar barStyle={'dark-content'}/>
          <Routing/>
        </SafeAreaView>
     </PersistGate>
    </Provider>
  );
};
export default App;
