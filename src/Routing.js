import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native'
import Login from './stateManagenent/screen/Login'
import Register from './stateManagenent/screen/Register'
import Home from './stateManagenent/screen/Home'
import About from './stateManagenent/screen/About'
import Berhasil from './stateManagenent/screen/Berhasil'
import MyTabs from './stateManagenent/screen/Botnav'
import Checkout from './stateManagenent/screen/Checkout'
import Detail from './stateManagenent/screen/Detail'
import FormPemesanan from './stateManagenent/screen/FormPemesanan'
import GunakanKode from './stateManagenent/screen/GunakanKode'
import Keranjang from './stateManagenent/screen/Keranjang'
import KodePesanan from './stateManagenent/screen/KodePesanan'
import KodePromo from './stateManagenent/screen/KodePromo'
import Setting from './stateManagenent/screen/Setting'
import Summary from './stateManagenent/screen/Summary'
import Transaksi from './stateManagenent/screen/Transaksi'
import AddData from './stateManagenent/screen/AddData';
import Splashscreen from './stateManagenent/screen/Splashscreen';



const Stack = createNativeStackNavigator();
export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {/* <Stack.Screen name="Home" component={HomeScreen}/> */}
        <Stack.Screen name="Splashscreen" component={Splashscreen} options={{headerShown:false}} />
        <Stack.Screen name="Login" component={Login}  />
        <Stack.Screen name="Botnav" component={MyTabs} options={{headerShown:false}}/>
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Home" component={Home} options={{headerShown:false}}/>
        <Stack.Screen name="Detail" component={Detail}  />
        <Stack.Screen name="FormPemesanan" component={FormPemesanan}  />
        <Stack.Screen name="Keranjang" component={Keranjang}  />
        <Stack.Screen name="Summary" component={Summary}  />
        <Stack.Screen name="Berhasil" component={Berhasil}/>
        {/* <Stack.Screen name="Transaksi" component={Transaksi} /> */}
        <Stack.Screen name="KodePesanan" component={KodePesanan} />
        {/* <Stack.Screen name="About" component={About} /> */}
        <Stack.Screen name="Setting" component={Setting} />
        <Stack.Screen name="Checkout" component={Checkout} />
        <Stack.Screen name="KodePromo" component={KodePromo} />
        <Stack.Screen name="GunakanKode" component={GunakanKode} />
        <Stack.Screen name="AddData" component={AddData} options={{headerShown:false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
