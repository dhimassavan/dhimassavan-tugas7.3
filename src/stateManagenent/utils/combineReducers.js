import {combineReducers} from 'redux'
import homeReducer from './reducer/homeReducer';
import cartReducer from './reducer/cartReducer';
import userReducer from './reducer/userReducer';
import transaksiReducer from './reducer/transaksiReducer';

const rootReducer = combineReducers({
   
   home : homeReducer,
   pemesanan : cartReducer,
   register : userReducer,
   transaksi: transaksiReducer 
})

export default rootReducer;
