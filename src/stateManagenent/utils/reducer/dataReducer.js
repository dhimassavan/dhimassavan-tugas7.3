const initialState = {
  product: [],
};
const dataReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_DATA': 
          return {
            ...state,
            product: action.data,
          };
        case 'UPDATE_DATA':
          // ambil data product sebelumnya
          var newData = [...state.product];
          // mencari key , key = id
          var findIndex = state.product.findIndex((value) => {
            return value.id === action.data.id;
          });
          //ganti data berdasarkan indexnya dengan data yg baru
          newData[findIndex] = action.data;
          return {
            ...state,
            product: newData,
          };
          case 'DELETE_DATA':
            var newData=[...state.product];
            // mencari key
            var findIndex = state.product.findIndex((value)=> { return value.id === action.id})
          // hapus data berdasarkan index dengan splice 
          newData.splice(findIndex,1)
          return{
            ...state,
            product: newData,
          }
    
        default:
          return state;
      } 
      
};

export default dataReducer;
