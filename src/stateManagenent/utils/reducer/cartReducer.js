const initialState = {
    pemesanan: [],
  };
  const cartReducer = (state = initialState, action) => {
      switch (action.type) {
          case 'ADD_DATA_CART': 
            return {
              ...state,
              pemesanan: action.data,
            };
          case 'UPDATE_DATA':
            // ambil data product sebelumnya
            var newData = [...state.product];
            // mencari key , key = id
            var findIndex = state.product.findIndex((value) => {
              return value.id === action.data.id;
            });
            //ganti data berdasarkan indexnya dengan data yg baru
            newData[findIndex] = action.data;
            return {
              ...state,
              product: newData,
            };
            case 'DELETE_PEMESANAN':
            return {
              ...state,
              pemesanan: [],
            };
          default:
            return state;
        } 
        
  };
  
  export default cartReducer;
  