const initialState = {
    transaksi: [],
  };
  const transaksiReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_DATA_TRANS': 
          return {
            ...state,
            transaksi: action.data,
          };
        case 'UPDATE_DATA':
          // ambil data product sebelumnya
          var newData = [...state.product];
          // mencari key , key = id
          var findIndex = state.product.findIndex((value) => {
            return value.id === action.data.id;
          });
          //ganti data berdasarkan indexnya dengan data yg baru
          newData[findIndex] = action.data;
          return {
            ...state,
            product: newData,
          };
          case 'DELETE_TRANS':
            return {
              ...state,
              transaksi: [],
            };
          default:
            return state;
      
      } 
      
};

export default transaksiReducer;
