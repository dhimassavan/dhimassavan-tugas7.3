const initialState = {
    register: {
      nama: '',
      email: '',
      password: '',
      phoneNumber: '',
      addressUser: '',
    }
  };
  const userReducer = (state = initialState, action) => {
      switch (action.type) {
          case 'ADD_DATA_USER': 
            return {
              ...state,
              register: action.data,
            };
          default:
            return state;
        } 
  };
  
  export default userReducer;
  