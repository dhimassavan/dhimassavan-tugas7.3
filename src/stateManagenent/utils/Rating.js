import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export const Rating = ({
	style,
	icon,
	iconSize = 15,
	colorOn = 'yellow',
	colorOff = 'grey',
	rating = 0,
	setRating,
	paddingHorizontal = 0,
}) => {

	const ratingFunction = (
		val = 1,
		starOn = 'yellow',
		starOff = 'grey',
	  ) => {
		let okStar = '';
		let okGrey = '';
		let i;
		for (i = 0; i < val; i++) {
		  okStar += starOn + '|';
		}
		for (i = 0; i < 5 - val; i++) {
		  okGrey += starOff + '|';
		}
		let starGrey = okStar + okGrey;
		let showStar = starGrey.split('|');
		return showStar;
	  };

	return (
		<View style={[styles.container, style]}>
			<TouchableOpacity onPress={setRating ? ()=> setRating(1) : ()=>{}} style={{paddingHorizontal: paddingHorizontal}}>
				<FontAwesome name="star" size={iconSize} color={ratingFunction(rating, colorOn, colorOff)[0]} />
			</TouchableOpacity>
			<TouchableOpacity onPress={setRating ? ()=> setRating(2) : ()=>{}} style={{paddingHorizontal: paddingHorizontal}}>
				<FontAwesome name="star" size={iconSize} color={ratingFunction(rating, colorOn, colorOff)[1]} />
			</TouchableOpacity>
			<TouchableOpacity onPress={setRating ? ()=> setRating(3) : ()=>{}} style={{paddingHorizontal: paddingHorizontal}}>
				<FontAwesome name="star" size={iconSize} color={ratingFunction(rating, colorOn, colorOff)[2]} />
			</TouchableOpacity>
			<TouchableOpacity onPress={setRating ? ()=> setRating(4) : ()=>{}} style={{paddingHorizontal: paddingHorizontal}}>
				<FontAwesome name="star" size={iconSize} color={ratingFunction(rating, colorOn, colorOff)[3]} />
			</TouchableOpacity>
			<TouchableOpacity onPress={setRating ? ()=> setRating(5) : ()=>{}} style={{paddingHorizontal: paddingHorizontal}}>
				<FontAwesome name="star" size={iconSize} color={ratingFunction(rating, colorOn, colorOff)[4]} />
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
	},
});