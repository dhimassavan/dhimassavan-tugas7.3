import { View, Text, Image } from 'react-native'
import React from 'react'
import { useEffect } from 'react';

const Splashscreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('Login');
    }, 1000);
  }, []);
  return (
    <View style={{flex:1, justifyContent:'center',backgroundColor: '#fff'}}>
      <Image style={{alignItems:'center', alignSelf:'center'}} source={require('../assets/JF.png')}/>
    </View>
  )
}

export default Splashscreen

// import { View, Text ,Image } from 'react-native'
// import React from 'react'
// import { useEffect } from 'react';


//   return (
//     <View>
//       <Image source={require('../assets/JK.png')}/>
//     </View>
//   )
// }

// export default Splashscreen;