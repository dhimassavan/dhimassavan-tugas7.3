import {
  View,
  Text,
  ScrollView,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment';
import Icon from 'react-native-vector-icons/AntDesign';

const FormPemesanan = ({navigation, route}) => {
  var dataToko = route.params;
  console.log('dataToko', dataToko);
  const [merk, setMerk] = useState('');
  const [color, setColor] = useState('');
  const [size, setSize] = useState('');
  const [note, setNote] = useState('');
  const [imageProduct, setImageProduct] = useState('');
  const [service, setService] = useState('');
  // const [service,setService] = useState('')
  // console.log(service);
  const {pemesanan} = useSelector(state => state.pemesanan);

  const dispatch = useDispatch();
  const storeData = () => {
    console.log('data', pemesanan);
    var dataPemesanan = [...pemesanan];
    const cart = {
      idCart: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      color: color,
      imageProduct: imageProduct,
      merk: merk,
      note: note,
      size: size,
      service: service,
    };
    dataPemesanan.push(cart);
    console.log('tambah data', dataPemesanan);
    dispatch({type: 'ADD_DATA_CART', data: dataPemesanan});
    navigation.navigate('Keranjang', dataToko);
  };
  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
      <ScrollView>
        <View style={{}}>
          <Text style={{marginLeft: 20, marginTop: 15, color: '#BB2427'}}>
            {' '}
            Merek
          </Text>
          <View
            style={{
              width: 355,
              height: 45,
              backgroundColor: '#F6F8FF',
              marginLeft: 20,
              marginTop: 5,
            }}>
            <TextInput
              style={{}}
              placeholder="Masukkan Merk Barang"
              onChangeText={text => setMerk(text)}
            />
          </View>
          <Text style={{marginLeft: 20, marginTop: 15, color: '#BB2427'}}>
            {' '}
            Warna
          </Text>
          <View
            style={{
              width: 355,
              height: 45,
              backgroundColor: '#F6F8FF',
              marginLeft: 20,
              marginTop: 5,
            }}>
            <TextInput
              style={{}}
              placeholder="Warna Barang, cth : Merah - Putih "
              onChangeText={text => setColor(text)}
            />
          </View>
          <Text style={{marginLeft: 20, marginTop: 15, color: '#BB2427'}}>
            {' '}
            Ukuran
          </Text>
          <View
            style={{
              width: 355,
              height: 45,
              backgroundColor: '#F6F8FF',
              marginLeft: 20,
              marginTop: 5,
            }}>
            <TextInput
              style={{}}
              placeholder="Cth : S, M, L / 39,40"
              onChangeText={text => setSize(text)}
            />
          </View>
          <Text style={{marginLeft: 20, marginTop: 15, color: '#BB2427'}}>
            {' '}
            Photo
          </Text>
          <View
            style={{
              width: 355,
              height: 45,
              backgroundColor: '#F6F8FF',
              marginLeft: 20,
              marginTop: 5,
            }}>
            <TextInput
              style={{}}
              placeholder="Masukan Link Foto"
              onChangeText={text => setImageProduct(text)}
            />
          </View>
          <TouchableOpacity
            style={{marginTop: 20}}
            onPress={() => setService('Ganti Sol Sepatu')}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 10,
              }}>
              <View
                style={{
                  marginLeft: 20,
                  width: 24,
                  height: 24,
                  borderColor: '#B8B8B8',
                  borderWidth: 1,
                  borderRadius: 3,
                  backgroundColor: service == 'Ganti Sol Sepatu' ? 'red' : null,
                }}></View>
              <Text style={{marginLeft: 20}}>Ganti Sol Sepatu</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setService('Ganti Tali Sepatu')}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 10,
              }}>
              <View
                style={{
                  marginLeft: 20,
                  width: 24,
                  height: 24,
                  borderColor: '#B8B8B8',
                  borderWidth: 1,
                  borderRadius: 3,
                  backgroundColor:
                    service == 'Ganti Tali Sepatu' ? 'red' : null,
                }}></View>
              <Text style={{marginLeft: 20}}>Ganti Tali Sepatu</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setService('Repaint Sepatu')}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 10,
              }}>
              <View
                style={{
                  marginLeft: 20,
                  width: 24,
                  height: 24,
                  borderColor: '#B8B8B8',
                  borderWidth: 1,
                  borderRadius: 3,
                  backgroundColor: service == 'Repaint Sepatu' ? 'red' : null,
                }}></View>
              <Text style={{marginLeft: 20}}>Repaint Sepatu</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setService('Cuci Sepatu')}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 10,
              }}>
              <View
                style={{
                  marginLeft: 20,
                  width: 24,
                  height: 24,
                  borderColor: '#B8B8B8',
                  borderWidth: 1,
                  borderRadius: 3,
                  backgroundColor: service == 'Cuci Sepatu' ? 'red' : null,
                }}></View>
              <Text style={{marginLeft: 20}}>Cuci Sepatu</Text>
            </View>
          </TouchableOpacity>
          <Text style={{marginLeft: 20, marginTop: 15, color: '#BB2427'}}>
            {' '}
            Catatan
          </Text>
          <View
            style={{
              width: 355,
              height: 96,
              backgroundColor: '#F6F8FF',
              marginLeft: 20,
              marginTop: 5,
            }}>
            <TextInput
              style={{}}
              placeholder="Cth : ingin ganti sol baru"
              onChangeText={text => setNote(text)}
            />
          </View>
          <Text style={{marginLeft: 20, marginTop: 15, color: '#BB2427'}}>
            {' '}
            Kupon Promo
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('KodePromo',dataToko)}
            style={{
              backgroundColor: '#FFFFFF',
              width: 355,
              height: 45,
              marginLeft: 20,
              marginTop: 10,
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
              borderRadius: 9,
              borderWidth: 1,
              borderColor: '#C03A2B',
              marginBottom: 10,
            }}>
            <View>
              <Text style={{fontSize: 13, fontWeight: '400',marginLeft:10}}>
                Pilih Kode Promo
              </Text>
            </View>
            <View>
            <Icon name="right" size={20} color="red" style={{marginRight:10}} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={storeData}
            style={{
              backgroundColor: '#BB2427',
              width: 355,
              height: 45,
              marginLeft: 20,
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 9,
              marginBottom: 40,
            }}>
            <Text style={{color: 'white', fontSize: 16, fontWeight: '700'}}>
              Masukkan Keranjang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default FormPemesanan;
