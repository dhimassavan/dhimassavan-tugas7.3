import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Button, 
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

const AddData = ({navigation, route}) => {
  console.log(route.params);
  const dataToko = route.params;
  console.log('dataToko',dataToko);
  const [storeName, setStoreName] = useState('');
  const [address, setAddress] = useState('');
  const [ratings, setRatings] = useState('');
  const [minimumPrice, setMinimumPrice] = useState('');
  const [maximumPrice, setMaximumPrice] = useState('');
  const [description, setDesciption] = useState('');
  const [isOpen, setIsOpen] = useState('');
  const [showBukaPicker, setShowBukaPicker] = useState(false);
  const [showTutupPicker, setShowTutupPicker] = useState(false);
  const [jamBuka, setJamBuka] = useState('');
  const [jamTutup, setJamTutup] = useState('');
  const checkData = () => {
    // pengecekan data
    if (dataToko) {
      setStoreName(dataToko.storeName);
      setAddress(dataToko.address);
      setRatings(dataToko.ratings);
      setMinimumPrice(dataToko.minimumPrice);
      setMaximumPrice(dataToko.maximumPrice);
      setDesciption(dataToko.description);
      setShowBukaPicker(dataToko.showBukaPicker);
      setShowTutupPicker(dataToko.showTutupPicker);
      setJamBuka(dataToko.jamBuka);
      setJamTutup(dataToko.jamTutup);
      setIsOpen(dataToko.isOpen)
    }
  };
  useEffect(() => {
    checkData();
}, [])

  const {toko} = useSelector(state => state.home);

  const dispatch = useDispatch();

  const storeData = () => {
    console.log('data', toko);
    var dataToko = [...toko];
    const data = {
      idStore: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      storeName: storeName,
      address: address,
      ratings: ratings,
      jamBuka:jamBuka,
      jamTutup:jamTutup,
      favourite: false,
      isOpen: isOpen,
      minimumPrice: minimumPrice,
      maximumPrice: maximumPrice,
      storeImage:
        'https://i2-prod.liverpoolecho.co.uk/incoming/article13975332.ece/ALTERNATES/s1227b/JS137694918.jpg',
      description: description,
    };
    dataToko.push(data);
    console.log('tambah data', dataToko);
    dispatch({type: 'ADD_DATA', data: dataToko});
    navigation.goBack();
  };
  const updateData= ()=>{ 
    console.log('data', toko);
    const data = {
      idStore: dataToko.idStore,
      storeName: storeName,
      address: address,
      ratings: ratings,
      jamBuka:jamBuka,
      jamTutup:jamTutup,
      favourite: false,
      isOpen: isOpen,
      minimumPrice: minimumPrice,
      maximumPrice: maximumPrice,
      storeImage:
        'https://insideretail.asia/wp-content/uploads/2022/07/APLA_SEA_I_VIVO-WOMENOVERVIEW2.jpg',
      description: description,
    };
    dispatch({type:'UPDATE_DATAA', data})
    navigation.goBack()
  }
   const deleteData = async () =>{
    dispatch({type: 'DELETE_DATAA', idStore: dataToko.idStore})
    navigation.goBack()
  }

  const showBukaTimepicker = () => {
    setShowBukaPicker(true);
  };

  const showTutupTimepicker = () => {
    setShowTutupPicker(true);
  };

  const handleBukaTimeChange = (event, selectedTime) => {
    const currentDate = selectedTime || new Date();
    setShowBukaPicker(false);
    setJamBuka(moment(currentDate).format('HH:mm'));
  };

  const handleTutupTimeChange = (event, selectedTime) => {
    const currentDate = selectedTime || new Date();
    setShowTutupPicker(false);
    setJamTutup(moment(currentDate).format('HH:mm'));
  };


  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
       <ScrollView //component yang digunakan agar tampilan kita bisa discroll
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{paddingBottom: 10}}
         >
            
            <KeyboardAvoidingView //component yang digunakan untuk mengatur agar keyboard tidak menutupi
               behavior='padding' //tampilan form atau text input
               enabled
               keyboardVerticalOffset={-500}
            >
      <View style={styles.header}>
        <TouchableOpacity
          style={{
            width: '15%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.goBack()}>
          <Icon name="arrowleft" size={25} color="#fff" />
        </TouchableOpacity>
        <View
          style={{
            width: '70%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 20, fontWeight: '600', color: '#fff'}}>
            ADD STORE DATA
          </Text>
        </View>
        <View style={{width: '15%'}} />
      </View>
      <View
        style={{
          flex: 1,
          padding: 30,
        }}>
        <Text style={{marginBottom: -15, marginTop: -10, color: 'black'}}>
          Nama Toko
        </Text>
        <TextInput
          placeholder="Masukkan Nama Toko"
          style={styles.txtInput}
          value={storeName}
          onChangeText={text => setStoreName(text)}
        />
        <Text style={{marginBottom: -15, marginTop: 10, color: 'black'}}>
          Alamat
        </Text>
        <TextInput
          placeholder="Masukkan Alamat"
          style={[styles.txtInput, {}]}
          multiline
          value={address}
          onChangeText={text => setAddress(text)}
        />
        <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
          <View style={{marginRight: 200}}>
            <Text style={{marginTop: 10, color: 'black'}}>Buka</Text>
            <TouchableOpacity
              onPress={() => setIsOpen('Buka')}
              style={{
                height: 30,
                width: 30,
                borderWidth: 2,
                borderRadius: 15,
                backgroundColor: isOpen == 'Buka' ? '#BB2427' : 'white',
              }}>
              {isOpen == 'Buka' ? (
                <Icon name="check" size={25} color="white" />
              ) : null}
            </TouchableOpacity>
          </View>
          <View style={{}}>
            <Text style={{marginTop: 10, color: 'black'}}>Tutup</Text>
            <TouchableOpacity
              onPress={() => setIsOpen('Tutup')}
              style={{
                height: 30,
                width: 30,
                borderWidth: 2,
                borderRadius: 15,
                backgroundColor: isOpen == 'Tutup' ? '#BB2427' : 'white',
              }}>
              {isOpen == 'Tutup' ? (
                <Icon name="check" size={25} color="white" />
              ) : null}
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 10,
            justifyContent: 'space-between',
          }}>
          <View style={{}}>
          <Text style={{ color: 'black'}}>Jam Buka</Text>
            <TouchableOpacity
              onPress={showBukaTimepicker}
              style={{marginTop:5,width: 130, height: 45, borderWidth:1,borderBlockColor:'#dedede',justifyContent:'center',alignItems:'center', borderRadius:6}}>
              <Text style={{margin:10,color:'black'}}>{jamBuka? jamBuka :'Pilih Jam'} </Text>
            </TouchableOpacity>
            {showBukaPicker && (
              <DateTimePicker
                value={new Date()}
                mode="time" 
                is24Hour={true}
                display="clock"
                onChange={handleBukaTimeChange}
              />
            )}
          </View>
          <View>
          <Text style={{ color: 'black'}}>Jam Tutup</Text>
            <TouchableOpacity onPress={showTutupTimepicker}  style={{marginTop:5,width: 130, height: 45, borderWidth:1,borderBlockColor:'#dedede',justifyContent:'center',alignItems:'center', borderRadius:6}}>
              <Text style={{margin:10, color:'black'}}>{jamTutup? jamTutup:'Pilih Jam'}</Text>
            </TouchableOpacity>
            {showTutupPicker && (
              <DateTimePicker
                value={new Date()}
                mode="time"
                is24Hour={true}
                display="clock"
                onChange={handleTutupTimeChange}
              />
            )}
          </View>
        </View>
        <Text style={{marginBottom: -15, marginTop: 10, color: 'black'}}>
          Ratings
        </Text>
        <TextInput
          placeholder="Ratings"
          style={styles.txtInput}
          onChangeText={text => setRatings(text)}
          keyboardType='number-pad'
          value={ratings}
        />
        <View style={{flexDirection: 'row' ,justifyContent:'space-between'}}>
          <View style={{marginRight:0}}>
            <Text style={{marginBottom: -15, marginTop: 10, color: 'black'}}>
              Harga Minimal
            </Text>
            <TextInput
              placeholder="Harga Minimal"
              style={styles.txtInput}
              value={minimumPrice}
              onChangeText={text => setMinimumPrice(text)}
              keyboardType='number-pad'
            />
          </View>
          <View>
            <Text style={{marginBottom: -15, marginTop: 10, color: 'black'}}>
              Harga Maximal
            </Text>
            <TextInput
              placeholder="Harga Maximal"
              style={styles.txtInput}
              value={maximumPrice}
              onChangeText={text => setMaximumPrice(text)}
              keyboardType='number-pad'
            />
          </View>
        </View>
        <Text style={{marginBottom: -15, marginTop: 10, color: 'black'}}>
          Deskripsi
        </Text>
        <TextInput
          placeholder="Deskripsi"
          style={styles.txtInput}
          value={description}
          onChangeText={text => setDesciption(text)}
        />
        
        <TouchableOpacity onPress={() => {
            if (route.params) {
              updateData();
            } else {
              storeData();
            }
          }}
           style={styles.btnAdd}>
          <Text style={{fontSize: 14, color: '#fff', fontWeight: '600'}}>
            {route.params ? 'Ubah Data' : 'Tambah Data'}
          </Text>
        </TouchableOpacity>
        {route.params && (
          <TouchableOpacity
          onPress={deleteData}
            style={[styles.btnAdd, {backgroundColor: '#dd2c00'}]}>
            <Text style={{fontSize: 14, color: '#fff', fontWeight: '600'}}>
              Hapus
            </Text>
          </TouchableOpacity>
        )}
      </View>
      </KeyboardAvoidingView>
         </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    backgroundColor: '#BB2427',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  txtInput: {
    width: '100%',
    borderRadius: 6,
    borderColor: 'black',
    borderWidth: 1,
    paddingHorizontal: 10,
    marginTop: 20,
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#BB2427',
    paddingVertical: 15,
  },
});

export default AddData;
