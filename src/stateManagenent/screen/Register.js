import {View, Text, Image, TextInput, Touchable, TouchableOpacity, ScrollView} from 'react-native';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const Register = ({navigation,route}) => {
  const [nama,setNama]= useState('')
  const [email,setEmail]= useState('')
  const [password,setPassword]= useState('')
  const [phoneNumber,setPhoneNumber]= useState('')
  const [addressUser,setAddressUser]= useState('')

  const {register} = useSelector(state => state.register);

  const dispatch = useDispatch();

  const storeData = () => {
    console.log('data', register);
    const user = {
      nama: nama,
      email: email,
      password: password,
      phoneNumber: phoneNumber,
      addressUser: addressUser
    };
    dispatch({type: 'ADD_DATA_USER', data: user});
    navigation.navigate('Login');
  };
  return (
    <View style={{flex: 1}}>
        <ScrollView>

      <Image
        style={{width: '100%', height: 317}}
        source={require('../assets/RectangleCopy.png')}
      />
      <View
        style={{
          width: '100%',
          height: 700,
          backgroundColor: '#FFFF',
          borderRadius: 19,
          marginTop: -180,
        }}>
          <Text style={{fontSize:24, fontWeight:'700',marginTop:15, marginLeft:20,color:'#0A0827'}}> WELLCOME,</Text>
          <Text style={{fontSize:24, fontWeight:'700',marginTop:3, marginLeft:20,color:'#0A0827'}}> Please Register</Text>
        <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Nama</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Enter Your Name" onChangeText={text => setNama(text)} />
      </View>
        <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Email</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Youremail@mail.com" onChangeText={text => setEmail(text)}/>
      </View>
        <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Password</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Enter Password" onChangeText={text => setPassword(text)}/>
      </View>
        <Text style={{marginLeft:20,marginTop:10 , color:'#BB2427'}}>Phone Number</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop:5,
          
        }}>
        <TextInput style={{}} placeholder="Your Phone Number"onChangeText={text => setPhoneNumber(text)} />
      </View>
        <Text style={{marginLeft:20,marginTop:10 , color:'#BB2427'}}>Address</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop:5,
          
        }}>
        <TextInput style={{}} placeholder="Your Address" onChangeText={text => setAddressUser(text)}/>
      </View>
     <View style={{flexDirection:'row',marginLeft:20,marginTop:10}}>
     <Image
        style={{marginRight:10}}
        source={require('../assets/Bitmap.png')}
        />
       <Image 
        style={{marginRight:10}}
        source={require('../assets/Bitmap-1.png')}
        />
       <Image
        style={{}}
        source={require('../assets/Bitmap-2.png')}
      /> 
      
     </View>
     <TouchableOpacity onPress={storeData} style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          marginLeft:20,
          marginTop:40,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:9}}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
        Register
      </Text>
     </TouchableOpacity>
     <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', marginTop:50}}>
     <Text style={{}}>
          Already have account?
        </Text>
          <TouchableOpacity >
            <Text style={{color:'#BB2427'}}>Login?</Text>
          </TouchableOpacity>
     </View>
        </View>
          </ScrollView>
    </View>
  );
};

export default Register;
