import {View, Text, ScrollView ,TextInput, Image, TouchableOpacity} from 'react-native';
import React from 'react';

const GunakanKode = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
      <ScrollView>
        <View style={{}}>
          <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Merek</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Masukkan Merk Barang" />
      </View>
          <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Warna</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Warna Barang, cth : Merah - Putih " />
      </View>
          <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Ukuran</Text>
        <View
        style={{
          width: 355,
          height: 45,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Cth : S, M, L / 39,40" />
      </View>
      <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Photo</Text>
        <View style={{width:84,height:84,margin:20, borderWidth:1, justifyContent:'center',alignItems:'center',borderColor:'#BB2427', borderRadius:8}}>
            <Image style={{width:25, height:25}} source={require('../assets/camera.png')}/>
            <Text style={{color:'#BB2427'}}>Add Photo</Text>
        </View>
        <View style={{flexDirection:'row', alignItems:'center', marginBottom:10}}>
            <View style={{marginLeft:20,width:24,height:24, borderColor:'#B8B8B8',borderWidth:1, borderRadius:3}}></View>
            <Text style={{marginLeft:20}} >Ganti Sol Sepatu</Text>
        </View>
        <View style={{flexDirection:'row', alignItems:'center', marginBottom:10}}>
            <View style={{marginLeft:20,width:24,height:24, borderColor:'#B8B8B8',borderWidth:1, borderRadius:3}}></View>
            <Text style={{marginLeft:20}} >Ganti Sol Sepatu</Text>
        </View>
        <View style={{flexDirection:'row', alignItems:'center', marginBottom:10}}>
            <View style={{marginLeft:20,width:24,height:24, borderColor:'#B8B8B8',borderWidth:1, borderRadius:3}}></View>
            <Text style={{marginLeft:20}} >Ganti Sol Sepatu</Text>
        </View>
        <View style={{flexDirection:'row', alignItems:'center', marginBottom:10}}>
            <View style={{marginLeft:20,width:24,height:24, borderColor:'#B8B8B8',borderWidth:1, borderRadius:3}}></View>
            <Text style={{marginLeft:20}} >Ganti Sol Sepatu</Text>
        </View>
        <Text style={{marginLeft:20,marginTop:15 , color:'#BB2427'}}> Catatan</Text>
        <View
        style={{
          width: 355,
          height: 96,
          backgroundColor: '#F6F8FF',
          marginLeft:20,
          marginTop: 5,
          
        }}>
        <TextInput style={{}} placeholder="Cth : ingin ganti sol baru" />
      </View>
      <TouchableOpacity onPress={()=> navigation.navigate('KodePromo')} style={{backgroundColor:'White',width: 355,
          height: 45,
          marginLeft:20,
          marginTop:20,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:9,
          borderWidth:0.5,
          borderColor:'#C03A2B'
          }}>
            <View style={{flexDirection:'row' ,justifyContent:'space-around'}}>
        <Text style={{color:'black',fontSize:12, marginLeft:10}}>
          DISKON 50 %
        </Text>
        <Image source={require('../assets/arow.png')} style={{marginLeft:200}}/>
            </View>
     </TouchableOpacity>
      <TouchableOpacity onPress={()=> navigation.navigate('Keranjang')} style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          marginLeft:20,
          marginTop:20,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:9,
          marginBottom:40}}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
      Masukkan Keranjang
      </Text>
     </TouchableOpacity>
        </View>

      </ScrollView>
    </View>
  );
};

export default GunakanKode;
