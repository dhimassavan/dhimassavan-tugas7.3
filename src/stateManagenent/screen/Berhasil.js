import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { useSelector } from 'react-redux';

const Berhasil = ({navigation,route}) => {
  const {transaksi} = useSelector(state => state.transaksi);
  console.log('data transaksi', transaksi);
  return (
    <View style={{backgroundColor:'#FFFF', flex:1,width:'100%',alignItems: 'center'}}>
        <View style={{marginTop:170}}>
        <Text style={{color:'#11A84E', fontSize:18, alignSelf:'center'}}>Reservasi Berhasil</Text>
        <Image style={{width:134, height:134,alignSelf:'center'}} source={require('../assets/checklist.png')}/>
        <Text style={{color:'black', fontSize:18, alignSelf:'center'}}>Kami Telah Mengirimkan Kode.{'\n'} Reservasi ke Menu Transaksi</Text>
        </View>

        <TouchableOpacity onPress={()=> navigation.navigate('Transaksi')} style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          position:'absolute',
          bottom:40,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:9}}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
      Reservasi Sekarang
      </Text>
     </TouchableOpacity>
      
    </View>
  )
}

export default Berhasil