import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  FlatList,
} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import MyTabs from './Botnav';
import {useEffect} from 'react';
import {Rating} from '../utils/Rating';

const Home = ({navigation}) => {
  const {toko} = useSelector(state => state.home);
  console.log('toko', toko);
  const {register} = useSelector(state => state.register);
  console.log('user', register);

  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
        <ScrollView>
      <View style={{width: '100%', height: 270, backgroundColor: '#FFFFFF',marginTop:-15}}>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
            marginTop: 40,
            justifyContent: 'space-around',
          }}>
          <Image
            style={{width: 45, height: 45}}
            source={require('../assets/Rectangle-3.png')}
          />
          <Image
            style={{marginLeft: 300}}
            source={require('../assets/Bag.png')}
          />
        </View>
        <Text style={{color: '#0A0827', marginTop: 10, marginLeft: 8,fontWeight:'700',fontSize:16}}>
          {'Hallo !!  '}
          {register.nama}
        </Text>
        <Text
          style={{
            fontSize: 20,
            fontWeight: '700',
            color: '#0A0827',
            marginTop: 10,
            marginLeft: 8,
          }}>
          {' '}
          Ingin merawat dan memperbaiki
        </Text>
        <Text
          style={{
            fontSize: 20,
            fontWeight: '700',
            color: '#0A0827',
            marginTop: 0,
            marginLeft: 8,
          }}>
          {' '}
          sepatumu? Cari disini
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              marginTop: 20,
              width: 275,
              height: 45,
              backgroundColor: '#F6F8FF',
              borderRadius: 14,
              justifyContent: 'center',
            }}>
            <Image
              style={{marginLeft: 8}}
              source={require('../assets/Search.png')}
            />
          </View>
          <View
            style={{
              marginTop: 20,
              marginLeft: 40,
              width: 50,
              height: 45,
              backgroundColor: '#F6F8FF',
              borderRadius: 14,
              justifyContent: 'center',
            }}>
            <Image
              style={{alignSelf: 'center'}}
              source={require('../assets/Filter.png')}
            />
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: 95,
            height: 95,
            backgroundColor: '#FFFFFF',
            marginTop: 20,
            borderRadius: 14,
            justifyContent: 'center',
          }}>
          <Image
            style={{alignSelf: 'center', width: 60, height: 60}}
            source={require('../assets/Group17.png')}
          />
          <Text
            style={{color: '#BB2427', alignSelf: 'center', fontWeight: '600'}}>
            Sepatu
          </Text>
        </View>
        <View
          style={{
            width: 95,
            height: 95,
            backgroundColor: '#FFFFFF',
            marginTop: 20,
            borderRadius: 14,
            justifyContent: 'center',
            marginLeft: 27,
            marginRight: 27,
          }}>
          <Image
            style={{alignSelf: 'center', width: 60, height: 60}}
            source={require('../assets/Group18.png')}
          />
          <Text
            style={{color: '#BB2427', alignSelf: 'center', fontWeight: '600'}}>
            Jaket
          </Text>
        </View>

        <View
          style={{
            width: 95,
            height: 95,
            backgroundColor: '#FFFFFF',
            marginTop: 20,
            borderRadius: 14,
            justifyContent: 'center',
          }}>
          <Image
            style={{alignSelf: 'center', width: 60, height: 60}}
            source={require('../assets/Group19.png')}
          />
          

          <Text
            style={{color: '#BB2427', alignSelf: 'center', fontWeight: '600'}}>
            Tas
          </Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 15,
          fontWeight: '700',
        }}>
        <View style={{marginRight: 130}}>
          <Text style={{fontWeight: '700'}}>Rekomensasi Terdekat</Text>
        </View>
        <View>
          <Text style={{fontWeight:'600'}}>View All</Text>
        </View>
      </View>
      <FlatList
        data={toko}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity onPress={() => navigation.navigate('Detail', item)}>
            <View
              style={{
                alignSelf: 'center',
                borderRadius: 5,
                // justifyContent: 'space-around',
                alignItems: 'center',
                marginTop: 15,
                width: 355,
                height: 135,
                backgroundColor: '#FFFFFF',
                flexDirection: 'row',
              }}>
              <TouchableOpacity onPress={()=> navigation.navigate('AddData',item)}>
                <View style={{marginLeft: 15}}>
                  <Image
                    source={{uri: item.storeImage}}
                    style={{width: 80, height: 121}}
                    />
                </View>
              </TouchableOpacity>
              <View style={{marginLeft: 15}}>
                <View style={{flexDirection: 'row'}}>
                  <Rating rating={item.ratings} colorOn="#FFC107" />
                </View>

                <Text style={{marginTop: 5}}>{item.ratings}</Text>
                <Text style={{marginTop: 0, color: 'black'}}>
                  {item.storeName}
                </Text>
                <Text style={{marginTop: 0, fontSize: 11,width:240}}>{item.address}</Text>
                <View
                  style={{
                    borderRadius: 30,
                    marginTop: 10,
                    backgroundColor:
                    item.isOpen == 'Buka' ? '#11A84E1F' : '#E64C3C33',
                    width: 58,
                    height: 21,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: item.isOpen == 'Buka' ? '#11A84E' : '#EA3D3D',
                    }}>
                    {' '}
                    {item.isOpen}
                  </Text>
                </View>
              </View>
              <View style={{top: 10, right: 10, position: 'absolute'}}>
                <TouchableOpacity>
                <Image source={require('../assets/lmerah.png')} />
                </TouchableOpacity>
              </View>
            </View>
          </TouchableOpacity>
        )}
        />
        
        </ScrollView>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.btnFloating}
        onPress={() => navigation.navigate('AddData')}>
        <Icon name="plus" size={25} color="#fff" />
      </TouchableOpacity>

    </View>
  );
};
const styles = StyleSheet.create({
  btnFloating: {
    position: 'absolute',
    bottom: 15,
    right: 30,
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Home;
