import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from './Home';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {Props} from 'react-native-image-zoom-viewer/built/image-viewer.type';
import Detail from './Detail';
import Transaksi from './Transaksi';
import About from './About';
import Login from './Login';
import Icon from 'react-native-vector-icons/AntDesign';

const Tab = createBottomTabNavigator();
function MyTabBar({state, descriptors, navigation}) {
  return (
    <View
      style={{
        flexDirection: 'row',
        height: 50,
        width: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };
        const labelIcon = {
          Home: 'home',
          Transaksi: 'filetext1',
          About: 'user',
        };
        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{flex: 1}}>
           
            <Icon
              name={labelIcon[label]}
              size={22}
              color={isFocused ? '#BB2427' : '#D8D8D8'}
              style={{alignSelf: 'center'}}
            />

            <Text
              style={{
                color: isFocused ? '#BB2427' : '#D8D8D8',
                alignSelf: 'center',
              }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}
function MyTabs() {
  return (
    <Tab.Navigator tabBar={props => <MyTabBar {...props} />}>
      {/* <Tab.Screen name="Login" component={Login} /> */}
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Transaksi" component={Transaksi} />
      <Tab.Screen name="About" component={About} />
    </Tab.Navigator>
  );
}
export default MyTabs;
