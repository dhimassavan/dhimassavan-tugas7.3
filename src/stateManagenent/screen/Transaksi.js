import { View, Text ,Image, TouchableOpacity, ScrollView,FlatList} from 'react-native'
import React from 'react'
import { useSelector } from 'react-redux';


const Transaksi = ({navigation,route}) => {
  const {transaksi} = useSelector(state => state.transaksi);
  console.log('data transaksi', transaksi);
  return (
    <View
    style={{
      flex: 1,
      backgroundColor: '#F6F8FF',
      alignItems: 'center',
      width: '100%',
    }}>
      <FlatList
      data={transaksi}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({item, index}) => (
    <TouchableOpacity onPress={()=> navigation.navigate('KodePesanan',item)}>

    <View
      style={{
          marginTop:5,
          width: '100%',
          height:'auto',
        borderRadius: 8,
        backgroundColor: '#FFFFFF',
      }}>
      <View style={{margin:20,}}>
        <Text style={{marginBottom: 5}}>20 Desember 2020  09:00</Text>
        <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
        {item.cartData[0].merk}
        </Text>
        <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
        {item.cartData[0].service}
        </Text>
        <View style={{flexDirection:'row'}}>
        <Text style={{fontWeight: 'bold',color:'#201F26'}}>
        Kode Reservasi : {item.reservationCode}
        </Text>
        <View style={{ marginLeft:100, width:81,height:21, backgroundColor:'rgba(242, 156, 31, 0.16)', borderRadius:10}}>
            <Text style={{color:'#FFC107', alignSelf:'center'}}>Reserved</Text>
        </View>
        </View>
        
      </View>
    </View>
          </TouchableOpacity>
           )}
      />

    </View>
  )
}

export default Transaksi