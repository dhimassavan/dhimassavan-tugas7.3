import {
  View,
  Text,
  ScrollView,
  TextInput,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

const Keranjang = ({navigation, route}) => {
  var dataToko = route.params;
  console.log('data Toko', dataToko)
  const {pemesanan} = useSelector(state => state.pemesanan);
  console.log('cart', pemesanan);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        alignItems: 'center',
        width: '100%',
      }}>
      <View style={{height: '80%'}}>
        <FlatList
          data={pemesanan}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity>
              <View
                style={{
                  marginTop: 20,
                  width: 353,
                  height: 135,
                  borderRadius: 8,
                  backgroundColor: '#FFFFFF',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  style={{width: 80, height: 121, margin: 20}}
                  source={{uri: item.imageProduct}}
                />
                <View>
                  <Text style={{marginBottom: 10, fontWeight: 'bold'}}>
                    {item.merk} - {item.color} - {item.size}
                  </Text>
                  <Text style={{marginBottom: 10}}>{item.service}</Text>
                  <Text>{item.note}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
      <TouchableOpacity onPress={()=> navigation.navigate('FormPemesanan')}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
        }}>
        <View
          style={{
            width: 20,
            height: 20,
            borderWidth: 1,
            borderColor: 'white',
            borderRadius: 5,
          }}>
          <Image
            style={{width: 15, height: 15, margin: 1.5}}
            source={require('../assets/plus.png')}
          />
        </View>
        <Text style={{color: '#BB2427', marginLeft: 10}}>Tambah Barang</Text>
      </View>
      </TouchableOpacity>


      <TouchableOpacity
        onPress={() => navigation.navigate('Checkout',dataToko)}
        style={{
          backgroundColor: '#BB2427',
          width: 355,
          height: 45,
          position: 'absolute',
          bottom: 40,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 9,
        }}>
        <Text style={{color: 'white', fontSize: 16, fontWeight: '700'}}>
          Selanjutnya
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Keranjang;
