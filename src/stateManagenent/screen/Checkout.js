import {View, Text, Image, TouchableOpacity,ScrollView, FlatList} from 'react-native';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

const Checkout = ({navigation,route}) => {
  var dataToko = route.params;
  console.log('data Toko', dataToko)
  const {pemesanan} = useSelector(state => state.pemesanan);
  console.log('cart', pemesanan);
  const {register} = useSelector(state => state.register);
  console.log('user register', register);
  console.log('cart', pemesanan);
  const {transaksi} = useSelector(state => state.transaksi);
  console.log('user register', transaksi);

  const dispatch = useDispatch();
  const storeData = () => {
    // console.log('data', pemesanan);
    var dataTransaksi = [...transaksi];
    const data = {
      idTransaction: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      reservationCode:moment(new Date()).format('DDHHmmss'),
      customerData: register, 
      storeData: dataToko,
      cartData:pemesanan
    };
    dataTransaksi.push(data);
    console.log('tambah data', dataTransaksi);
    dispatch({type: 'ADD_DATA_TRANS', data: dataTransaksi});
    dispatch({type: 'DELETE_PEMESANAN'});
    navigation.navigate('Berhasil');
  }
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        alignItems: 'center',
        width: '100%',
      }}>
        <ScrollView>

        
      <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 135,
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
        }}>
        <View style={{margin:20,}}>
          <Text style={{marginBottom: 5}}>Data Customer</Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
          {register.nama} {register.phoneNumber}
          </Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
          {register.addressUser}
          </Text>
          <Text style={{fontWeight: 'bold',color:'#201F26'}}>
        {register.email}
          </Text>
        </View>
      </View>
      <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 'auto',
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
        }}>
        <View style={{margin:20,}}>
          <Text style={{marginBottom: 5}}>Alamat Outlet Tujuan</Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
          {dataToko.storeName}
          </Text>
          <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
         {dataToko.address}
          </Text>
          
        </View>
      </View>
      <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 'auto',
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
          
        }}>
            <Text style={{marginLeft:20, marginTop:5}}>Barang</Text>
       


     
      <FlatList
          data={pemesanan}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity>
              <View
                style={{
                  marginTop: 20,
                  width: 353,
                  height: 135,
                  borderRadius: 8,
                  backgroundColor: '#FFFFFF',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  style={{width: 80, height: 121, margin: 20}}
                  source={{uri: item.imageProduct}}
                />
                <View>
                  <Text style={{marginBottom: 10, fontWeight: 'bold'}}>
                    {item.merk} - {item.color} - {item.size}
                  </Text>
                  <Text style={{marginBottom: 10}}>{item.service}</Text>
                  <Text>{item.note}</Text>
                </View>
                
              </View>
              <View style={{justifyContent:'space-around',flexDirection:'row'}}>
        
        <Text style={{ marginTop:5}}> 1 Pasang</Text>
        <Text style={{marginLeft:170,color:'black'}}>RP @50.000</Text>
      </View>
            </TouchableOpacity>
          )}
        />
    </View>
    <View
      style={{
        marginTop: 5,
        width: '100%',
        height: 'auto',
        borderRadius: 8,
        backgroundColor: '#FFFFFF',
      }}>
      <View style={{margin:20}}>
        <Text style={{marginBottom: 5, marginTop:-10}}>Rincian Pembayaran</Text>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <Text>Cuci Sepatu</Text>
          <Text style={{color:'#FFC107',marginRight:100}}>X1 Pasang</Text>
          <Text>RP 50.000</Text>
        </View>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <Text>Biaya Antar</Text>
          <Text>RP 3.000</Text>
        </View>
          <View style={{borderWidth:0.2}}></View>
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <Text>Total</Text>
          <Text>RP 53.000</Text>
        </View>
        
        {/* <Text style={{fontWeight: 'bold',marginBottom: 3 ,color:'#201F26'}}>
        Jack Repair - Seturan (027-343457)
        </Text>
        <Text style={{fontWeight: 'bold',marginBottom: 3,color:'#201F26'}}>
        Jl. Affandi No 18, Sleman, Yogyakarta
        </Text> */}
        
      </View>
    </View>
    <View
        style={{
          marginTop: 5,
          width: '100%',
          height: 135,
          borderRadius: 8,
          backgroundColor: '#FFFFFF',
        }}>
        <View style={{margin:20, width:'auto'}}>
          <Text style={{marginTop:-10}}>Pilih Pembayaran</Text>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <View style={{flexDirection:'row'}}>
             <View style={{marginRight:15,width:128,height:82,borderWidth:1,borderColor:'#E1E1E1',justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../assets/bankT.png')}/>
              <Text>Transfer Bank</Text>
             </View>
             <View style={{marginRight:15,width:128,height:82,borderWidth:1,borderColor:'#E1E1E1',justifyContent:'center',alignItems:'center'}}>
              <Image style={{width:40,height:40}} source={require('../assets/dana.png')}/>
              <Text>DANA</Text>
             </View>
             <View style={{marginRight:15,width:128,height:82,borderWidth:1,borderColor:'#E1E1E1',justifyContent:'center',alignItems:'center'}}>
              <Image style={{}} source={require('../assets/ovo.png')}/>
              <Text style={{marginTop:15}}>OVO</Text>
             </View>
             <View style={{marginRight:15,width:128,height:82,borderWidth:1,borderColor:'#E1E1E1',justifyContent:'center',alignItems:'center'}}>
              <Image source={require('../assets/bankT.png')}/>
              <Text>Transfer Bank</Text>
             </View>
          </View>
            </ScrollView>
        </View>
      </View>
    <TouchableOpacity onPress={storeData} style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          marginTop:20,
          marginBottom:40,
          justifyContent:'center',
          alignItems:'center',
          alignSelf:'center',
          borderRadius:9,
          }}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
      Pesan Sekarang
      </Text>
     </TouchableOpacity>
     </ScrollView>
    </View>
  );
};

export default Checkout;
