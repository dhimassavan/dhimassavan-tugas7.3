import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'



const About = ({navigation}) => {
  return (
    <View style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        alignItems: 'center',
        width: '100%',}}>
       
       <Image style={{width:'100%', height:300}} source={require('../assets/Wprofile.png')}/>
       <View style={{ backgroundColor: '#FFFFFF',marginTop:10,width:350,height:'auto'}}>
        <Text style={{marginLeft:50,fontSize:22,fontWeight:'bold',marginBottom:10}}>About</Text>
        <Text style={{marginLeft:50,fontSize:22,fontWeight:'bold',marginBottom:10}}>Terms & Condition</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Summary')}>
        <Text style={{marginLeft:50,fontSize:22,fontWeight:'bold',marginBottom:10}}>FAQ</Text>
        </TouchableOpacity>
        <Text style={{marginLeft:50,fontSize:22,fontWeight:'bold',marginBottom:10}}>History</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Setting')}>
        <Text style={{marginLeft:50,fontSize:22,fontWeight:'bold',marginBottom:10}}>Setting</Text>
        </TouchableOpacity>
       </View>
       <TouchableOpacity
        onPress={() => navigation.navigate('Login')}
        style={{
          backgroundColor: '#FFFFFF',
          width: 355,
          height: 45,
          position: 'absolute',
          bottom: 40,
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 9,
        }}>
        <Text style={{color: '#BB2427', fontSize: 16, fontWeight: '700'}}>
          Log Out
        </Text>
      </TouchableOpacity>
    </View>
  )
}

export default About