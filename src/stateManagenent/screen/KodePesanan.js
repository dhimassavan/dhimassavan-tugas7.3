import { View, Text , Image, TouchableOpacity,FlatList} from 'react-native'
import React from 'react'
import { useSelector } from 'react-redux';


const KodePesanan = ({navigation,route}) => {
  var kodePesanan = route.params;
  console.log(kodePesanan);
  // const {transaksi} = useSelector(state => state.transaksi);
  // console.log('data transaksi', transaksi);
  return (
    <View style={{
        flex: 1,
        backgroundColor: '#F6F8FF',
        alignItems: 'center',
        width: '100%'}}>
          
      <View style={{width:'100%', height:'auto', backgroundColor:'#FFFFFF',alignItems: 'center'}}>
      <Text style={{marginTop:10}}>20 Desember 2020  09:00</Text>
      <Text style={{marginTop:10,color:'black',fontWeight:'bold', fontSize:20}}>{kodePesanan.idTransaction}</Text>
      <Text style={{marginTop:10 ,fontWeight:'bold', fontSize:15}}>Sebutkan Kode Reservasi saat </Text>
      <Text style={{marginTop:10 , fontSize:15,fontWeight:'bold',marginBottom:20}}>tiba di outlet </Text>
      </View>
      <View  style={{height:300}}>
      <FlatList
          data={kodePesanan.cartData}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity>
              <View
                style={{
                  marginTop: 20,
                  width: 353,
                  height: 135,
                  borderRadius: 8,
                  backgroundColor: '#FFFFFF',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  style={{width: 80, height: 121, margin: 20}}
                  source={{uri: item.imageProduct}}
                />
                <View>
                  <Text style={{marginBottom: 10, fontWeight: 'bold'}}>
                    {item.merk} - {item.color} - {item.size}
                  </Text>
                  <Text style={{marginBottom: 10}}>{item.service}</Text>
                  <Text>{item.note}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
        </View>
      <Text style={{margin:20 , alignSelf:'flex-start',color:'black',fontWeight:'bold'}}>Status Pesanan</Text>
        <View style={{justifyContent:'space-around',alignItems:'center' ,flexDirection:'row',width:353,height:78, backgroundColor:'#FFFFFF',borderRadius:10}}>
            <View style={{width:10,height:10, borderRadius:5, backgroundColor:'#BB2427'}}></View>
            <View>
            <Text style={{color:'black',fontWeight:'bold'}}>Telah Reservasi</Text>
            <Text>20 Desember 2020</Text>
            </View>
            <Text>09.00</Text>
        </View>
        
    </View>
  )
}

export default KodePesanan