import {View, Text, Image, ImageBackground , TouchableOpacity } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Rating } from '../utils/Rating';
import { convertCurrency } from '../utils/helper';

const Detail = ({navigation, route}) => {
  var dataToko = route.params;
  console.log('data Toko', dataToko)
  return (
    <View style={{flex: 1}}>
      <ImageBackground
        source={{uri:dataToko.storeImage}}
        style={{width: '100%', height: 316, alignItems:'flex-end'}}>
          <TouchableOpacity onPress={()=> navigation.navigate('Keranjang',dataToko)}>
        <Icon name="shopping-bag" size={30} color="#fff" style={{margin:20}} /> 
          </TouchableOpacity>
      </ImageBackground>

      <View
        style={{
          backgroundColor: '#FFFF',
          width: '100%',
          height: '100%',
          marginTop: -80,
          borderRadius: 20,
        }}>
          <Text style={{margin:20,fontSize:22, fontWeight:'700', color:'#201F26'}}>
          {dataToko.storeName}
          </Text>
          <View style={{flexDirection: 'row'}}>
                  <Rating style={{marginLeft:20}} rating={dataToko.ratings} colorOn="#FFC107" />
                </View>
          <View style={{flexDirection:'row', marginLeft:20,marginTop:15, alignItems:'center', justifyContent:'space-between'}}>
            <Image source={require('../assets/location_outline.png')} style={{marginRight:10}}/>
            
            <Text style={{color:'#201F26',margin:5,marginLeft:-10, width:270}} >
            {dataToko.address}
            </Text>
            <View style={{}}>
            <Text style={{ color:'#3471CD',right:10}}>
              Lihat Maps
            </Text>
            </View>
          </View>
          <View style={{ borderBottomWidth: 1, borderColor: '#EEEEEE' , marginTop:10 }}>
         </View>
         <Text style={{color:'#201F26', marginLeft:20, marginTop:10, fontWeight:'700', fontSize:17}}>
         Deskripsi
         </Text>
         <Text style={{color:'#595959', marginLeft:20, marginTop:10}}>
         {dataToko.description}
         </Text>
         <Text style={{color:'#201F26', marginLeft:20, marginTop:10, fontWeight:'700',fontSize:17}}>
         Range Biaya
         </Text>
         <Text style={{color:'#595959', marginLeft:20, marginTop:10, fontWeight:'700',fontSize:15}}>
         RP. {convertCurrency(dataToko.minimumPrice, '')}- RP. {convertCurrency(dataToko.maximumPrice, '')}
         </Text>
         <Text style={{color:'#201F26', marginLeft:20, marginTop:10, fontWeight:'700',fontSize:17}}>
         Jam Oprasional
         </Text>
         <Text style={{color:'#595959', marginLeft:20, marginTop:10, fontWeight:'700',fontSize:15}}>
         {dataToko.jamBuka}-{dataToko.jamTutup}
         </Text>
         <TouchableOpacity onPress={()=> navigation.navigate('FormPemesanan',dataToko)} style={{backgroundColor:'#BB2427',width: 355,
          height: 45,
          marginLeft:20,
          marginTop:15,
          justifyContent:'center',
          alignItems:'center',
          borderRadius:9}}>
      <Text style={{color:'white',fontSize:16, fontWeight:'700'}}>
        Repair Disini
      </Text>
     </TouchableOpacity>
        </View>
    </View>
  );
};

export default Detail;
